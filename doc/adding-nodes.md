# Quick start

## How to create files for a new zone

*The documentation is written for the production cluster, the same apply for other clusters.*

Basically:
  - Create your `site` file in `cluster/prod/site/` folder
  - Create your `node` files in `cluster/prod/node/` folder
  - Add your wireguard configuration to `cluster/prod/cluster.nix`
    - You will have to edit your NAT config manually to bind one public IPv4 port to each node
    - Nodes' public wireguard keys are generated during the first run of `deploy_nixos`, see below
  - Add your nodes to `cluster/prod/ssh_config`, it will be used by the various SSH scripts.
    - If you use `ssh` directly, use `ssh -F ./cluster/prod/ssh_config`
    - Add `User root` for the first time as your user will not be declared yet on the system

## How to deploy a Nix configuration on a fresh node

We suppose that the node name is `datura`. 
Start by doing the deployment one node at a time, you will have plenty of time
in your operator's life to break everything through automation.

Run:
  - `./deploy_nixos prod datura` - to deploy the nix configuration file;
   - a new wireguard key is printed if it hadn't been generated before, it has to be
     added to `cluster.nix`, and then redeployed on all nodes as the new wireguard conf is needed everywhere
  - `./deploy_passwords prod datura` - to deploy user's passwords
   - if a user changes their password (using `./passwd`), needs to be redeployed on all nodes to setup the password on all nodes
  - `./deploy_pki prod datura` - to deploy Nomad's and Consul's PKI

