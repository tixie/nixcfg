job "albatros" {
  datacenters = ["neptune", "jupiter", "corrin", "bespin"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  constraint {
    attribute = "${attr.unique.hostname}"
    operator = "!="
    value = "caribou"
  }

  group "main" {
    network {
      port "http" { to = 8080 }
    }

    task "controller" {
      driver = "docker"
      config {
        image = "dxflrs/albatros:0.9.0-rc.3"
        ports = [ "http" ]
        volumes = [
          "secrets/certs:/var/run/secrets/albatros"
        ]
      }
      env {
        ALBATROS_URL = "https://albatros.staging.deuxfleurs.org"
        NOMAD_ADDR = "https://nomad.service.staging.consul:4646"
        NOMAD_CACERT = "/var/run/secrets/albatros/nomad-ca.crt"
        NOMAD_CLIENT_CERT = "/var/run/secrets/albatros/nomad-client.crt"
        NOMAD_CLIENT_KEY = "/var/run/secrets/albatros/nomad-client.key"
        CONSUL_HTTP_ADDR = "https://consul.service.staging.consul:8501"
        CONSUL_CACERT = "/var/run/secrets/albatros/consul-ca.crt"
        CONSUL_CLIENT_CERT = "/var/run/secrets/albatros/consul-client.crt"
        CONSUL_CLIENT_KEY = "/var/run/secrets/albatros/consul-client.key"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/certs/consul-ca.crt"
      }
      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/certs/consul-client.crt"
      }
      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/certs/consul-client.key"
      }

      template {
        data = "{{ key \"secrets/nomad/nomad-ca.crt\" }}"
        destination = "secrets/certs/nomad-ca.crt"
      }
      template {
        data = "{{ key \"secrets/nomad/nomad-client.crt\" }}"
        destination = "secrets/certs/nomad-client.crt"
      }
      template {
        data = "{{ key \"secrets/nomad/nomad-client.key\" }}"
        destination = "secrets/certs/nomad-client.key"
      }

      resources {
        cpu    = 200
        memory = 200
      }

      service {
        name = "albatros"
        tags = [
          "albatros",
          "tricot albatros.staging.deuxfleurs.org",
          "d53-cname albatros.staging.deuxfleurs.org",
        ]
        port = "http"
        check {
          type = "tcp"
          port = "http"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
