#FROM rust:1.69-bullseye as builder
FROM debian:bullseye-slim as builder

RUN apt-get update && \
    apt-get install -y \
      pkg-config \
      git \
      curl \
      postgresql \
      postgresql-contrib \
      libpq-dev \
      gettext \
      git \
      python \
      curl \
      gcc \
      make \
      openssl \
      libssl-dev \
      libclang-dev \
	  libsqlite3-dev

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain nightly -y
ENV PATH=/root/.cargo/bin:$PATH
RUN cargo install wasm-pack

ARG PLUME_VERSION
WORKDIR /opt
RUN git clone -n https://git.joinplu.me/lx/Plume.git plume

WORKDIR /opt/plume
RUN git checkout ${PLUME_VERSION}
RUN git merge 397e3b4d9720475257817b322c05323d12918216
RUN rm rust-toolchain

WORKDIR /opt/plume/script
RUN chmod a+x ./wasm-deps.sh && ./wasm-deps.sh

WORKDIR /opt/plume
RUN chmod a+x ./script/plume-front.sh && ./script/plume-front.sh
RUN cargo install --path ./ --force --no-default-features --features sqlite,s3
RUN cargo install --path plume-cli --force --no-default-features --features sqlite,s3
RUN cargo clean

#-----------------------------
FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    libpq5 \
    libssl1.1 \
    rclone \
    fuse \
	sqlite3

WORKDIR /app

COPY --from=builder /opt/plume /app
COPY --from=builder /root/.cargo/bin/plm /usr/local/bin/
COPY --from=builder /root/.cargo/bin/plume /usr/local/bin/

CMD ["plume"]
