job "garage-staging" {
  type = "system"
  priority = 90

  datacenters = [ "neptune", "jupiter", "corrin", "bespin" ]

  update {
    max_parallel = 1
    stagger = "1m"
    min_healthy_time = "10s"
  }

  group "garage-staging" {
    network {
      port "s3" { static = 3990 }
      port "rpc" { static = 3991 }
      port "web" { static = 3992 }
      port "k2v" { static = 3993 }
      port "admin" { static = 3909 }
    }

    task "server" {
      driver = "nix2"

      config {
        packages = [
          "#bash", # so that we can enter a shell inside container
          "#coreutils",
          # garage v1.0.0-rc1 as of 2024-03-28
          "git+https://git.deuxfleurs.fr/Deuxfleurs/garage.git?ref=next-0.10&rev=afad62939e071621666ca7255f7164f92c4475bb"
        ]
        command = "garage"
        args = [ "server" ]
        bind = {
          "/mnt/storage/garage-staging/data" = "/data",
          "/mnt/ssd/garage-staging/meta" = "/meta",
        }
      }

      env = {
        RUST_LOG = "garage=info,garage_api=debug",
      }

      # files currently owned by root, we don't want to chown everything
      user = "root"

      template {
        data = file("../config/garage.toml")
        destination = "etc/garage.toml"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "etc/garage/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "etc/garage/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "etc/garage/consul-client.key"
      }

      resources {
        memory = 1500
        memory_max = 3000
        cpu = 500
      }

      kill_signal = "SIGINT"
      kill_timeout = "20s"

      service {
        name = "garage-staging-rpc"
        tags = ["garage-staging-rpc"]
        port = "rpc"
      }

      service {
        name = "garage-staging-s3-api"
        tags = [
          "garage-staging-api",
          "tricot garage.staging.deuxfleurs.org",
          "tricot-add-header Access-Control-Allow-Origin *",
          "tricot-site-lb",
        ]
        port = "s3"
        check {
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-staging-k2v-api"
        tags = [
          "garage-staging-k2v-api",
          "tricot k2v.staging.deuxfleurs.org",
          "tricot-add-header Access-Control-Allow-Origin *",
          "tricot-site-lb",
        ]
        port = "k2v"
        check {
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-staging-web"
        tags = [
            "garage-staging-web",
            "tricot *.web.staging.deuxfleurs.org",
            "tricot staging.deuxfleurs.org",
            "tricot matrix.home.adnab.me/.well-known/matrix/server",
            "tricot-add-header Access-Control-Allow-Origin *",
            "tricot-site-lb",
        ]
        port = "web"
        check {
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-staging-admin"
        tags = [
          "garage-staging-admin",
        ]
        port = "admin"
        check {
          name = "garage-admin-health-check"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 10
            grace = "90s"
            ignore_warnings = true
          }
        }
        check {
          name = "garage-tcp-liveness-check"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = true
          }
        }
      }

      restart {
        interval = "5m"
        attempts = 10
        delay    = "1m"
        mode     = "delay"
      }
    }
  }
}
