job "directory" {
  datacenters = ["neptune", "jupiter", "corrin", "bespin"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "bottin" {
    count = 1

    network {
      port "ldap_port" {
        static = 389
      }
    }

    task "bottin" {
      driver = "nix2"
      config {
        packages = [
          "git+https://git.deuxfleurs.fr/Deuxfleurs/bottin.git?ref=main&rev=9cab98d2cee386ece54b000bbdf2346da8b55eed"
        ]
        command = "bottin"
      }
      user = "root" # needed to bind port 389

      resources {
        memory = 100
      }

      template {
        data = file("../config/bottin/config.json.tpl")
        destination = "config.json"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "etc/bottin/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "etc/bottin/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "etc/bottin/consul-client.key"
      }

      template {
        data = <<EOH
CONSUL_HTTP_ADDR=https://localhost:8501
CONSUL_HTTP_SSL=true
CONSUL_CACERT=/etc/bottin/consul-ca.crt
CONSUL_CLIENT_CERT=/etc/bottin/consul-client.crt
CONSUL_CLIENT_KEY=/etc/bottin/consul-client.key
EOH
        destination = "secrets/env"
        env = true
      }

      service {
        tags = ["bottin"]
        port = "ldap_port"
        name = "bottin"
        check {
          type = "tcp"
          port = "ldap_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }

  group "guichet" {
    count = 1

    network {
      port "web_port" { static = 9991 }
    }

    task "guichet" {
      driver = "nix2"
      config {
        packages = [
          "git+https://git.deuxfleurs.fr/Deuxfleurs/guichet.git?ref=main&rev=10bdee10cf6947ec6dd0ba5040d7274d6c3316a7"
        ]
        command = "guichet"
      }

      template {
        data = file("../config/guichet/config.json.tpl")
        destination = "config.json"
      }

      resources {
        memory = 200
      }

      service {
        name = "guichet"
        tags = [
          "guichet",
          "tricot guichet.staging.deuxfleurs.org",
          "d53-cname guichet.staging.deuxfleurs.org",
        ]
        port = "web_port"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

