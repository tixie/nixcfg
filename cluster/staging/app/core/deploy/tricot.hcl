job "core-tricot" {
  datacenters = ["neptune", "jupiter", "corrin", "bespin"]
  type = "system"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 1
    stagger = "1m"
  }

  group "tricot" {
    network {
      port "http_port" { static = 80 }
      port "https_port" { static = 443 }
      port "metrics_port" { static = 9334 }
    }

    task "server" {
      driver = "nix2"

      config {
        packages = [
          "git+https://git.deuxfleurs.fr/Deuxfleurs/tricot.git?ref=redirect&rev=b76b6dcbcc47ebc61848389a6b0d5d4e8d8cde48"
        ]
        command = "tricot"
        # cap_add = [ "net_bind_service" ]  # this doesn't work for whatever reason, so we need to put user = "root" instead
      }
      user = "root"

      resources {
        cpu = 500
        memory = 200
      }

      restart {
        interval = "30m"
        attempts = 2
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "etc/tricot/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "etc/tricot/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "etc/tricot/consul-client.key"
      }

      template {
        data = <<EOH
TRICOT_NODE_NAME={{ env "attr.unique.consul.name" }}
TRICOT_LETSENCRYPT_EMAIL=alex@adnab.me
#TRICOT_ENABLE_COMPRESSION=true
TRICOT_CONSUL_HOST=https://localhost:8501
TRICOT_CONSUL_CA_CERT=/etc/tricot/consul-ca.crt
TRICOT_CONSUL_CLIENT_CERT=/etc/tricot/consul-client.crt
TRICOT_CONSUL_CLIENT_KEY=/etc/tricot/consul-client.key
TRICOT_HTTP_BIND_ADDR=[::]:80
TRICOT_HTTPS_BIND_ADDR=[::]:443
TRICOT_METRICS_BIND_ADDR=[::]:9334
RUST_LOG=tricot=debug
RUST_BACKTRACE=1
EOH
        destination = "secrets/env"
        env = true
      }

      service {
        name = "tricot-http"
        port = "http_port"
        tags = [
          "d53-aaaa ${attr.unique.hostname}.machine.staging.deuxfleurs.org",
          "d53-aaaa ${meta.site}.site.staging.deuxfleurs.org",
          "d53-aaaa staging.deuxfleurs.org",
          "(diplonat (tcp_port 80))"
        ]
        address_mode = "host"
      }

      service {
        name = "tricot-https"
        port = "https_port"
        tags = [
          "(diplonat (tcp_port 443))"
        ]
        address_mode = "host"
      }

      service {
        name = "tricot-metrics"
        port = "metrics_port"
        address_mode = "host"
      }
    }
  }
}
