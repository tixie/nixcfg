job "core-d53" {
  datacenters = ["neptune", "jupiter", "corrin", "bespin"]
  type = "service"
  priority = 90

  group "D53" {
    count = 1

    task "d53" {
      driver = "nix2"

      config {
        packages = [
          "git+https://git.deuxfleurs.fr/lx/D53.git?ref=diplonat-autodiscovery&rev=49d94dae1d753c1f3349be7ea9bc7e7978c0af15"
        ]
        command = "d53"
      }

      resources {
        cpu = 100
        memory = 100
      }

      restart {
        interval = "3m"
        attempts = 10
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "etc/tricot/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "etc/tricot/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "etc/tricot/consul-client.key"
      }

      template {
        data = <<EOH
D53_CONSUL_HOST=https://localhost:8501
D53_CONSUL_CA_CERT=/etc/tricot/consul-ca.crt
D53_CONSUL_CLIENT_CERT=/etc/tricot/consul-client.crt
D53_CONSUL_CLIENT_KEY=/etc/tricot/consul-client.key
D53_PROVIDERS=deuxfleurs.org:gandi
D53_GANDI_API_KEY={{ key "secrets/d53/gandi_api_key" }}
D53_ALLOWED_DOMAINS=staging.deuxfleurs.org
RUST_LOG=d53=debug
EOH
        destination = "secrets/env"
        env = true
      }
    }
  }
}
