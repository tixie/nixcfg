job "telemetry-system" {
  datacenters = ["neptune", "jupiter", "corrin", "bespin"]
  type = "system"
  priority = "100"

  group "collector" {
    network {
      port "node_exporter" { static = 9100 }
    }

    task "node_exporter" {
      driver = "nix2"

      config {
        packages = [ "#prometheus-node-exporter" ]
        command = "node_exporter"
        args = [ "--path.rootfs=/host" ]
        bind_read_only = {
          "/" = "/host"
        }
      }

      resources {
        cpu = 50
        memory = 40
      }

      service {
        name = "node-exporter"
        tags = [ "telemetry" ]
        port = "node_exporter"
        check {
          type = "http"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
