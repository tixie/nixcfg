{ config, pkgs, lib, ... } @ args:

{
  deuxfleurs.clusterName = "staging";

  # The IP range to use for the Wireguard overlay of this cluster
  deuxfleurs.clusterPrefix = "10.14.0.0/16";

  deuxfleurs.clusterNodes = {
    "caribou" = {
      siteName = "neptune";
      publicKey = "lABn/axzD1jkFulX8c+K3B3CbKXORlIMDDoe8sQVxhs=";
      address = "10.14.1.3";
      endpoint = "77.207.15.215:33723";
    };
    "origan" = {
      siteName = "jupiter";
      publicKey = "smBQYUS60JDkNoqkTT7TgbpqFiM43005fcrT6472llI=";
      address = "10.14.2.33";
      endpoint = "82.64.238.84:33733";
    };
    "piranha" = {
      siteName = "corrin";
      publicKey = "m9rLf+233X1VColmeVrM/xfDGro5W6Gk5N0zqcf32WY=";
      address = "10.14.3.1";
      #endpoint = "82.120.233.78:33721";
    };
    "df-pw5" = {
      siteName = "bespin";
      publicKey = "XLOYoMXF+PO4jcgfSVAk+thh4VmWx0wzWnb0xs08G1s=";
      address = "10.14.4.1";
      endpoint = "bitfrost.fiber.shirokumo.net:33734";
    };
  };

  deuxfleurs.wgautomeshPort = 1667;
  deuxfleurs.services.wgautomesh.logLevel = "debug";

  # Bootstrap IPs for Consul cluster,
  # these are IPs on the Wireguard overlay
  services.consul.extraConfig.retry_join = [
    "10.14.1.3"  # caribou
    "10.14.2.33" # origan
    "10.14.3.1"  # piranha
  ];

  deuxfleurs.adminAccounts = {
    lx = [
      # Keys for accessing nodes from outside
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJpaBZdYxHqMxhv2RExAOa7nkKhPBOHupMP3mYaZ73w9 lx@lindy"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIw+IIX8+lZX9RrHAbwi/bncLYStXpI4EmK3AUcqPY2O lx@kusanagi "
    ];
    quentin = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT1+H08FdUSvdPpPKdcafq4+JRHvFVjfvG5Id97LAoROmFRUb/ZOMTLdNuD7FqvW0Da5CPxIMr8ZxfrFLtpGyuG7qdI030iIRZPlKpBh37epZHaV+l9F4ZwJQMIBO9cuyLPXgsyvM/s7tDtrdK1k7JTf2EVvoirrjSzBaMhAnhi7//to8zvujDtgDZzy6aby75bAaDetlYPBq2brWehtrf9yDDG9WAMYJqp//scje/WmhbRR6eSdim1HaUcWk5+4ZPt8sQJcy8iWxQ4jtgjqTvMOe5v8ZPkxJNBine/ZKoJsv7FzKem00xEH7opzktaGukyEqH0VwOwKhmBiqsX2yN quentin@dufour.io"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBu+KUebaWwlugMC5fGbNhHc6IaQDAC6+1vMc4Ww7nVU1rs2nwI7L5qcWxOwNdhFaorZQZy/fJuCWdFbF61RCKGayBWPLZHGPsfqDuggYNEi1Qil1kpeCECfDQNjyMTK058ZBBhOWNMHBjlLWXUlRJDkRBBECY0vo4jRv22SvSaPUCAnkdJ9rbAp/kqb497PTIb2r1l1/ew8YdhINAlpYQFQezZVfkZdTKxt22n0QCjhupqjfh3gfNnbBX0z/iO+RvAOWRIZsjPFLC+jXl+n7cnu2cq1nvST5eHiYfXXeIgIwmeENLKqp+2Twr7PIdv22PnJkh6iR5kx7eTRxkNZdN quentin@deuxfleurs.fr"
    ];
    adrien = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINvUFN6HmZS5oxxOtmF6ug393m5NYbSbDI4G8pX6H9GZ adrien@pratchett"
    ];
    maximilien = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHMMR6zNzz8NQU80wFquhUCeiXJuGphjP+zNouKbn228GyESu8sfNBwnuZq86vblR11Lz8l2rtCM73GfAKg29qmUWUHRKWvRIYWv2vaUJcCdy0bAxIzcvCvjZX0SpnIKxe9y3Rp0LGO5WLYfw0ZFaavwFZP0Z8w1Kj9/zBmL2X2avbhkaYHi/C1yXhbvESYQysmqLa48EX/TS616MBrgR9zbI9AoTQ9NOHnR14Tve/AP/khcZoBJdm4hTttMbNkEc0wonzdylTDew263SPRs/uoqnQIpUtErdPHqU10Yup8HjXjEyFJsSwcZcM5sZOw5JKckKJwmcd0yjO/x/4/Mk5"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGioTNbjGE3KblbqhnkEWUfGkYZ2p5UAVqPdQJaUBWoo"
    ];
    trinity = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWGWTRoF5MjQ5bmFdQENQlNdoYtA7Wd61GM0TMHZDki"
    ];
    kokakiwi = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPTsEgcOtb2bij+Ih8eg8ZqO7d3IMiWykv6deMzlSSS kokakiwi@kira"
    ];
    baptiste = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnGkJZZrHIUp9q0DXmVLLuhCIe7Vu1J3j6dJ1z1BglqX7yOLdFQ6LhHXx65aND/KCOM1815tJSnaAyKWEj9qJ31RVUoRl42yBn54DvQumamJUaXAHqJrXhjwxfUkF9B73ZSUzHGADlQnxcBkmrjC5FkrpC/s4xr0o7/GIBkBdtZhX9YpxBfpH6wEcCruTOlm92E3HvvjpBb/wHsoxL1f2czvWe69021gqWEYRFjqtBwP36NYZnGOJZ0RrlP3wUrGCSHxOKW+2Su+tM6g07KPJn5l1wNJiOcyBQ0/Sv7ptCJ9+rTQNeVBMoXshaucYP/bKJbqH7dONrYDgz59C4+Kax"
    ];
    aeddis = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILoFf9fMYwLOpmiXKgn4Rs99YCj94SU1V0gwGXR5N4Md"
    ];
    boris = [
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPts/36UvMCFcx3anSMV8bQKGel4c4wCsdhDGWHzZHgg07DxMt+Wk9uv0hWkqLojkUbCl/bI5siftiEv6En0mHw="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJaD6flgTLkKimMB1qukiLKLVqsN+gizgajETjTwbscXEP2Fajmqy+90v1eXTDcGivmTyi8wOqkJ0s4D7dWP7Ck="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEIZKA/SIicXq7HPFJfumrMc1iARqA1TQWWuWLrguOlKgFPBVym/IVjtYGAQ/Xtv4wU9Ak0s+t9UKpQ/K38kVe0="
    ];
    vincent = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEve02acr522psrPxeElkwIPw2pc6QWtsUVZoaigqwZZ"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL/h+rxR2o+vN0hUWQPdpO7YY9aaKxO3ZRnUh9QiKBE7"
    ];
    armael = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJOoPghSM72AVp1zATgQzeLkuoGuP9uUTTAtwliyWoix"
    ];
    marion = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKzOhSTEOudBWCHi5wHc6MP0xjJJhuIDZEcx+hP6kz9N"
    ];
    darkgallium = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJX0A2P59or83EKhh32o8XumGz0ToTEsoq89hMbMtr7h"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB540H9kn+Ocs4Wjc1Y3f3OkHFYEqc5IM/FiCyoVVoh3"
    ];
  };

  # For Garage ipv6 communication
  networking.firewall.allowedTCPPorts = [ 3991 ];

  ## ===== EXPERIMENTAL SECTION FOR STAGING CLUSTER =====

  # Test nomad 1.6
  services.nomad.package = pkgs.nomad_1_6;
  nixpkgs.config.allowUnfree = true; # Accept nomad's BSL license

  # We're doing lots of experiments so GC periodically is usefull.
  nix.gc.automatic = true;

  imports = [
    ## ---- Nix Nomad jobs using nomad-driver-nix2 ----
    ({ pkgs, ... }: {
      services.nomad.extraSettingsPlugins = [
        (import ./nomad-driver-nix2.nix { inherit pkgs; })
      ];
      services.nomad.extraPackages = [
        pkgs.nix
        pkgs.git
      ];
      services.nomad.settings.plugin = [
        {
          "nix2-driver" = [
            {
              config = [
                {
                  default_nixpkgs = "github:nixos/nixpkgs/nixos-23.11";
                }
              ];
            }
          ];
        }
      ];
    })
    ## ---- Nix cache: use our cache on Garage (prod cluster) ----
    # Use our cache as additionnal substituer (this acts the same way for
    # our Nix packages than the Docker hub acts for our Docker images)
    ({ pkgs, ... }: {
      nix.settings.substituters = [ "https://nix.web.deuxfleurs.fr" ];
      nix.settings.trusted-public-keys = [ "nix.web.deuxfleurs.fr:eTGL6kvaQn6cDR/F9lDYUIP9nCVR/kkshYfLDJf1yKs=" ];
    })
  ];
}
