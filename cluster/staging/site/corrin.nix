{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "corrin";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.1";
  deuxfleurs.cnameTarget = "corrin.site.staging.deuxfleurs.org.";
  deuxfleurs.publicIPv4 = "109.222.162.50";
}
