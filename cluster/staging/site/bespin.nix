{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "bespin";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.5.254";
  deuxfleurs.cnameTarget = "bespin.site.staging.deuxfleurs.org.";
}
