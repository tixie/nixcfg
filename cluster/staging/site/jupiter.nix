{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "jupiter";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.1";
  deuxfleurs.cnameTarget = "jupiter.site.staging.deuxfleurs.org.";
}
