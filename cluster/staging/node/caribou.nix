# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "caribou";
  deuxfleurs.staticIPv6.address = "2001:910:1204:1::23";
  deuxfleurs.isRaftServer = true;

  system.stateVersion = "21.05";
}
