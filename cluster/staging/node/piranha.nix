# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "piranha";
  deuxfleurs.staticIPv4.address = "192.168.1.25";
  deuxfleurs.staticIPv6.address = "2a01:cb05:911e:ec00:223:24ff:feb0:ea82";

  system.stateVersion = "22.11";
}
