#!/usr/bin/env bash

# Bruxelles (bespin): git forge at git.deuxfleurs.fr

curl -vv  -X PUT http://localhost:8500/v1/catalog/register -H "Content-Type: application/json" --data @- <<EOF
{
  "Datacenter": "prod",
  "Node": "gitea",
  "Address": "192.168.5.200",
  "NodeMeta": {
    "site": "bespin",
    "cname_target": "bespin.site.deuxfleurs.fr."
  },
  "Service": {
    "Service": "gitea",
    "Tags": ["tricot git.deuxfleurs.fr"],
    "Address": "192.168.5.200",
    "Port": 3001
  }
}
EOF

# Lille (scorpio): ADRN's personnal services under luxeylab.net

curl -vv  -X PUT http://localhost:8500/v1/catalog/register -H "Content-Type: application/json" --data @- <<EOF
{
  "Datacenter": "prod",
  "Node": "spoutnik",
  "Address": "192.168.1.51",
  "NodeMeta": {
    "site": "scorpio",
    "cname_target": "scorpio.site.deuxfleurs.fr."
  },
  "Service": {
    "Service": "adrien-nginx",
    "Tags": [
      "tricot-https *.luxeylab.net",
      "tricot-https *.zinz.dev"
    ],
    "Address": "192.168.1.51",
    "Port": 443
  }
}
EOF
