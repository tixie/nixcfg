{ name ? "deuxfleurs/cryptpad"
, tag ? "nix-latest"
}: let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in rec {
  cryptpad = pkgs.callPackage ./default.nix {};
  docker = pkgs.callPackage ./docker.nix {
    inherit name tag;
    inherit cryptpad;
  };
}
