{ pkgs ? import <nixpkgs> {}

, name ? "cryptpad"
, tag ? "nix-latest"

, withOnlyOffice ? true

, cryptpad ? pkgs.callPackage ./default.nix { inherit withOnlyOffice; }
}: let
  cryptpad' = cryptpad.overrideAttrs {
    postInstall = ''
      ln -sf /cryptpad/customize $out/customize
    '';
  };
in pkgs.dockerTools.buildImage {
  inherit name tag;

  config = {
    Cmd = [
      (pkgs.lib.getExe cryptpad')
    ];

    Volumes = {
      "/cryptpad/customize" = {};
    };
  };
}
