/*
 * You can override the configurable values from this file.
 * The recommended method is to make a copy of this file (/customize.dist/application_config.js)
   in a 'customize' directory (/customize/application_config.js).
 * If you want to check all the configurable values, you can open the internal configuration file
   but you should not change it directly (/common/application_config_internal.js)
*/
define(['/common/application_config_internal.js'], function (AppConfig) {
    // To inform users of the support ticket panel which languages your admins speak:
    AppConfig.supportLanguages = [ 'en', 'fr' ];

    /* Select the buttons displayed on the main page to create new collaborative sessions.
     * Removing apps from the list will prevent users from accessing them. They will instead be
     * redirected to the drive.
     * You should never remove the drive from this list.
     */
    AppConfig.availablePadTypes = ['drive', 'teams', 'doc', 'presentation', 'pad', 'kanban', 'code', 'form', 'poll', 'whiteboard',
                                'file', 'contacts', 'slide', 'convert'];
    // disabled: sheet

    /* You can display a link to your own privacy policy in the static pages footer.
     * Since this is different for each individual or organization there is no default value.
     * See the comments above for a description of possible configurations.
     */
    AppConfig.privacy = {
      "default": "https://deuxfleurs.fr/CGU.html",
    };

    /* You can display a link to your instances's terms of service in the static pages footer.
     * A default is included for backwards compatibility, but we recommend replacing this
     * with your own terms.
     *
     * See the comments above for a description of possible configurations.
     */
    AppConfig.terms = {
      "default": "https://deuxfleurs.fr/CGU.html",
    };

    return AppConfig;
});
