docker run \
    --name coturn \
    --rm \
    -it \
    -v `pwd`/docker-entrypoint.sh:/usr/local/bin/docker-entrypoint.sh \
    --network=host \
    coturn/coturn:4.6.1-r2-alpine
