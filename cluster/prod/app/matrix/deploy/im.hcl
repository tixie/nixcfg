job "matrix" {
  datacenters = ["scorpio", "neptune"]
  type = "service"
  priority = 40

  group "matrix" {
    count = 1

    network {
      port "api_port" { static = 8008 }
      port "web_port" { to = 8043 }
    }

    task "synapse" {
      driver = "docker"

      config {
        image = "lxpz/amd64_synapse:v58"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "api_port" ]
        command = "python"
        args = [
          "-m", "synapse.app.homeserver",
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        volumes = [
          "secrets/conf:/etc/matrix-synapse",
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse-logs:/var/log/matrix-synapse",
          "/tmp/synapse:/tmp"
        ]
      }

      template {
        data = file("../config/synapse/homeserver.yaml")
        destination = "secrets/conf/homeserver.yaml"
      }

      template {
        data = file("../config/synapse/log.yaml")
        destination = "secrets/conf/log.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/server_name.yaml")
        destination = "secrets/conf/server_name.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/report_stats.yaml")
        destination = "secrets/conf/report_stats.yaml"
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.signing.key\" }}"
        destination = "secrets/conf/homeserver.signing.key"
      }

      env {
        SYNAPSE_CACHE_FACTOR = 1
      }

      resources {
        cpu = 1000
        memory = 500
        memory_max = 1000
      }

      service {
        name = "synapse"
        port = "api_port"
        address_mode = "host"
        tags = [
          "matrix",
          "tricot im.deuxfleurs.fr/_matrix 100",
          "tricot im.deuxfleurs.fr:443/_matrix 100",
          "tricot im.deuxfleurs.fr/_synapse 100",
          "tricot-add-header Access-Control-Allow-Origin *",
          "d53-cname im.deuxfleurs.fr",
        ]
        check {
          type = "tcp"
          port = "api_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }


    task "media-async-upload" {
      driver = "docker"

      config {
        image = "lxpz/amd64_synapse:v58"
        readonly_rootfs = true
        command = "/usr/local/bin/matrix-s3-async"
        work_dir = "/tmp"
        volumes = [
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse:/tmp"
        ]
      }

      resources {
        cpu = 100
        memory = 200
        memory_max = 500
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/chat/synapse/s3_access_key" | trimSpace }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/chat/synapse/s3_secret_key" | trimSpace }}
AWS_DEFAULT_REGION=garage
PG_USER={{ key "secrets/chat/synapse/postgres_user" | trimSpace }}
PG_PASS={{ key "secrets/chat/synapse/postgres_pwd" | trimSpace }}
PG_DB={{ key "secrets/chat/synapse/postgres_db" | trimSpace }}
PG_HOST={{ env "meta.site" }}.psql-proxy.service.2.cluster.deuxfleurs.fr
PG_PORT=5432
EOH
        destination = "secrets/env"
        env = true
      }
    }

    task "riotweb" {
      driver = "docker"
      config {
        image = "lxpz/amd64_elementweb:v35"
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/srv/http/config.json"
        ]
      }

      template {
        data = file("../config/riot_web/config.json")
        destination   = "secrets/config.json"
      }

      resources {
        memory = 21
      }

      service {
        tags = [
          "webstatic",
          "tricot im.deuxfleurs.fr 10",
          "tricot riot.deuxfleurs.fr 10",
          "d53-cname riot.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        name = "webstatic"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }

  group "syncv3" {
    count = 1

    network {
      port "syncv3_api" { to = 8009 }
      port "syncv3_metrics" { to = 2112 }
    }

    task "syncv3" {
      driver = "docker"

      config {
        image = "ghcr.io/matrix-org/sliding-sync:v0.99.12"
        ports = [ "syncv3_api", "syncv3_metrics" ]
      }

      resources {
        cpu = 1000
        memory = 500
        memory_max = 1000
      }

      template {
        data = <<EOH
SYNCV3_SERVER=http://synapse.service.prod.consul:8008
SYNCV3_DB=postgresql://{{ key "secrets/chat/syncv3/postgres_user"|trimSpace }}:{{ key "secrets/chat/syncv3/postgres_pwd"|trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul/{{ key "secrets/chat/syncv3/postgres_db"|trimSpace }}?sslmode=disable
SYNCV3_SECRET={{ key "secrets/chat/syncv3/secret"|trimSpace }}
SYNCV3_BINDADDR=0.0.0.0:8009
SYNCV3_PROM=0.0.0.0:2112
EOH
        destination = "secrets/env"
        env = true
      }

      service {
        name = "matrix-syncv3"
        port = "syncv3_api"
        address_mode = "host"
        tags = [
          "matrix",
          "tricot im-syncv3.deuxfleurs.fr 100",
          "tricot-add-header Access-Control-Allow-Origin *",
          "d53-cname im-syncv3.deuxfleurs.fr",
        ]
        check {
          type = "tcp"
          port = "syncv3_api"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "matrix-syncv3-metrics"
        port = "syncv3_metrics"
        address_mode = "host"
      }
    }
  }
}

