{
  "log_level": "info",
  "easybridge_avatar": "/app/easybridge.jpg",

  "web_bind_addr": "0.0.0.0:8281",
  "web_url": "https://easybridge.deuxfleurs.fr",
  "web_session_key": "{{ key "secrets/chat/easybridge/web_session_key" | trimSpace }}",

  "appservice_bind_addr": "0.0.0.0:8321",
  "registration": "/data/registration.yaml",
  "homeserver_url": "https://im.deuxfleurs.fr",
  "matrix_domain": "deuxfleurs.fr",
  "name_format": "{}_ezbr_",

  "db_type": "postgres",
  "db_path": "host=psql-proxy.service.2.cluster.deuxfleurs.fr port=5432 user={{ key "secrets/chat/easybridge/db_user" | trimSpace }} dbname=easybridge password={{ key "secrets/chat/easybridge/db_pass" | trimSpace }} sslmode=disable"
}
