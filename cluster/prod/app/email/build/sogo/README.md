```
docker build -t superboum/amd64_sogo:v6 .

# privileged is only for debug
docker run --rm -ti \
  --privileged \
  -p 8080:8080 \
  -v /tmp/sogo/log:/var/log/sogo \
  -v /tmp/sogo/run:/var/run/sogo \
  -v /tmp/sogo/spool:/var/spool/sogo \
  -v /tmp/sogo/tmp:/tmp \
  -v `pwd`/sogo:/etc/sogo:ro \
  superboum/amd64_sogo:v1
```

Password must be url encoded in sogo.conf for postgres
Will need a nginx instance: http://wiki.sogo.nu/nginxSettings

Might (or might not) be needed:
traefik.frontend.headers.customRequestHeaders=x-webobjects-server-port:443||x-webobjects-server-name=sogo.deuxfleurs.fr||x-webobjects-server-url:https://sogo.deuxfleurs.fr
