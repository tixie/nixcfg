hosts = {{ env "meta.site" }}.bottin.service.prod.consul
dn = {{ key "secrets/email/dovecot/ldap_binddn" | trimSpace }}
dnpass = {{ key "secrets/email/dovecot/ldap_bindpwd" | trimSpace }}
base = dc=deuxfleurs,dc=fr
scope = subtree
user_filter = (&(mail=%u)(&(objectClass=inetOrgPerson)(memberOf=cn=email,ou=groups,dc=deuxfleurs,dc=fr)))
pass_filter = (&(mail=%u)(&(objectClass=inetOrgPerson)(memberOf=cn=email,ou=groups,dc=deuxfleurs,dc=fr)))
user_attrs = \
  =user=%{ldap:cn}, \
  =mail=maildir:/var/mail/%{ldap:cn}, \
  =uid=1000, \
  =gid=1000 
