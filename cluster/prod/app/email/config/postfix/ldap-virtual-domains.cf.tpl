bind = yes
bind_dn = {{ key "secrets/email/postfix/ldap_binddn" | trimSpace }}
bind_pw = {{ key "secrets/email/postfix/ldap_bindpwd" | trimSpace }}
version = 3
timeout = 20
start_tls = no
tls_require_cert = no
server_host = ldap://{{ env "meta.site" }}.bottin.service.prod.consul
scope = sub
search_base = ou=domains,ou=groups,dc=deuxfleurs,dc=fr
query_filter = (&(objectclass=dNSDomain)(domain=%s))
result_attribute = domain
