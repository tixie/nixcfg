{
  WONoDetach = NO;
  WOWorkersCount = 3;
  SxVMemLimit = 600;
  WOPort = "127.0.0.1:20000";
  SOGoProfileURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_user_profile";
  OCSFolderInfoURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_folder_info";
  OCSSessionsFolderURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_sessions_folder";
  OCSEMailAlarmsFolderURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_alarms_folder";
  OCSStoreURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_store";
  OCSAclURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_acl";
  OCSCacheFolderURL = "postgresql://{{ key "secrets/email/sogo/postgre_auth" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/sogo/sogo_cache_folder";
  SOGoTimeZone = "Europe/Paris";
  SOGoMailDomain = "deuxfleurs.fr";
  SOGoLanguage = French;
  SOGoAppointmentSendEMailNotifications = YES;
  SOGoEnablePublicAccess = YES;
  SOGoMailingMechanism = smtp;
  SOGoSMTPServer = postfix-smtp.service.prod.consul;
  SOGoSMTPAuthenticationType = PLAIN;
  SOGoForceExternalLoginWithEmail = YES;
  SOGoIMAPAclConformsToIMAPExt = YES;
  SOGoTimeZone = UTC;
  SOGoSentFolderName = Sent;
  SOGoTrashFolderName = Trash;
  SOGoDraftsFolderName = Drafts;
  SOGoIMAPServer = "imaps://dovecot-imaps.service.prod.consul:993/?tlsVerifyMode=none";
  SOGoSieveServer = "sieve://sieve.service.prod.consul:4190/?tls=YES";
  SOGoIMAPAclConformsToIMAPExt = YES;
  SOGoVacationEnabled = NO;
  SOGoForwardEnabled = NO;
  SOGoSieveScriptsEnabled = NO;
  SOGoFirstDayOfWeek = 1;
  SOGoRefreshViewCheck = every_5_minutes;
  SOGoMailAuxiliaryUserAccountsEnabled = NO;
  SOGoPasswordChangeEnabled = YES;
  SOGoPageTitle = "deuxfleurs.fr";
  SOGoLoginModule = Mail;
  SOGoMailAddOutgoingAddresses = YES;
  SOGoSelectedAddressBook = autobook;
  SOGoMailAuxiliaryUserAccountsEnabled = YES;
  SOGoCalendarEventsDefaultClassification = PRIVATE;
  SOGoMailReplyPlacement = above;
  SOGoMailSignaturePlacement = above;
  SOGoMailComposeMessageType = html;

  SOGoLDAPContactInfoAttribute = "displayname";

  SOGoDebugRequests = YES;
  //SOGoEASDebugEnabled = YES;
  //ImapDebugEnabled = YES;
  LDAPDebugEnabled = YES;
  //MySQL4DebugEnabled = YES;
  PGDebugEnabled = YES;

  SOGoUserSources = (
    {
        type = ldap;
        CNFieldName = displayname;
        IDFieldName = cn;
        UIDFieldName = cn;
        MailFieldNames = (mail, mailForwardingAddress);
        SearchFieldNames = (displayname, cn, sn, mail, telephoneNumber);
        IMAPLoginFieldName = mail;
        baseDN = "ou=users,dc=deuxfleurs,dc=fr";
        bindDN = "{{ key "secrets/email/sogo/ldap_binddn" | trimSpace }}";
        bindPassword = "{{ key "secrets/email/sogo/ldap_bindpw" | trimSpace}}";
        bindFields = (cn, mail);
        canAuthenticate = YES;
        displayName = "Bottin";
        hostname = "ldap://{{ env "meta.site" }}.bottin.service.prod.consul:389";
        id = bottin;
        isAddressBook = NO;
    }
  );
}
