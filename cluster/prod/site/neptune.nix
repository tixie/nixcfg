{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "neptune";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.254";
  deuxfleurs.cnameTarget = "neptune.site.deuxfleurs.fr.";
  deuxfleurs.publicIPv4 = "77.207.15.215";
}
