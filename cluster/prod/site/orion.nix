{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "orion";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.254";
  deuxfleurs.cnameTarget = "orion.site.deuxfleurs.fr.";
  deuxfleurs.publicIPv4 = "82.66.80.201";
}
