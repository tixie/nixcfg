{ config, pkgs, ... } @ args:

{
  deuxfleurs.clusterName = "prod";

  # The IP range to use for the Wireguard overlay of this cluster
  deuxfleurs.clusterPrefix = "10.83.0.0/16";

  deuxfleurs.clusterNodes = {
    "concombre" = {
      siteName = "neptune";
      publicKey = "VvXT0fPDfWsHxumZqVShpS33dJQAdpJ1E79ZbCBJP34=";
      address = "10.83.1.1";
      endpoint = "82.67.87.112:33731";
    };
    "courgette" = {
      siteName = "neptune";
      publicKey = "goTkBJGmzrGDOAjUcdH9G0JekipqSMoaYQdB6IHnzi0=";
      address = "10.83.1.2";
      endpoint = "82.67.87.112:33732";
    };
    "celeri" = {
      siteName = "neptune";
      publicKey = "oZDAb8LoLW87ktUHyFFec0VaIar97bqq47mGbdVqJ0U=";
      address = "10.83.1.3";
      endpoint = "82.67.87.112:33733";
    };
    /*
    "dahlia" = {
      siteName = "orion";
      publicKey = "EtRoWBYCdjqgXX0L+uWLg8KxNfIK8k9OTh30tL19bXU=";
      address = "10.83.2.1";
      endpoint = "82.66.80.201:33731";
    };
    "diplotaxis" = {
      siteName = "orion";
      publicKey = "HbLC938mysadMSOxWgq8+qrv+dBKzPP/43OMJp/3phA=";
      address = "10.83.2.2";
      endpoint = "82.66.80.201:33732";
    };
    "doradille" = {
      siteName = "orion";
      publicKey = "e1C8jgTj9eD20ywG08G1FQZ+Js3wMK/msDUE1wO3l1Y=";
      address = "10.83.2.3";
      endpoint = "82.66.80.201:33733";
    };
    */
    "df-ykl" = {
      siteName = "bespin";
      publicKey = "bIjxey/VhBgVrLa0FxN/KISOt2XFmQeSh1MPivUq9gg=";
      address = "10.83.3.1";
      endpoint = "109.136.139.78:33731";
    };
    "df-ymf" = {
      siteName = "bespin";
      publicKey = "pUIKv8UBl586O7DBrHBsb9BgNU7WlYQ2r2RSNkD+JAQ=";
      address = "10.83.3.2";
      endpoint = "109.136.139.78:33732";
    };
    "df-ymk" = {
      siteName = "bespin";
      publicKey = "VBmpo15iIJP7250NAsF+ryhZc3j+8TZFnE1Djvn5TXI=";
      address = "10.83.3.3";
      endpoint = "109.136.139.78:33733";
    };
    "abricot" = {
      siteName = "scorpio";
      publicKey = "Sm9cmNZ/BfWVPFflMO+fuyiera4r203b/dKhHTQmBFg=";
      address = "10.83.4.1";
      endpoint = "82.65.41.110:33741";
    };
    "ananas" = {
      siteName = "scorpio";
      publicKey = "YC78bXUaAQ02gz0bApenM4phIo/oMPR78QCmyG0tay4=";
      address = "10.83.4.2";
      endpoint = "82.65.41.110:33742";
    };
  };

  # Pin Nomad version
  services.nomad.package = pkgs.nomad_1_6;
  nixpkgs.config.allowUnfree = true; # Accept nomad's BSL license

  # Bootstrap IPs for Consul cluster,
  # these are IPs on the Wireguard overlay
  services.consul.extraConfig.retry_join = [
    "10.83.1.1"  # concombre
    "10.83.2.1"  # dahlia
    "10.83.3.1"  # df-ykl
  ];

  deuxfleurs.adminAccounts = {
    lx = [
      # Keys for accessing nodes from outside
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJpaBZdYxHqMxhv2RExAOa7nkKhPBOHupMP3mYaZ73w9 lx@lindy"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIw+IIX8+lZX9RrHAbwi/bncLYStXpI4EmK3AUcqPY2O lx@kusanagi "
    ];
    quentin = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT1+H08FdUSvdPpPKdcafq4+JRHvFVjfvG5Id97LAoROmFRUb/ZOMTLdNuD7FqvW0Da5CPxIMr8ZxfrFLtpGyuG7qdI030iIRZPlKpBh37epZHaV+l9F4ZwJQMIBO9cuyLPXgsyvM/s7tDtrdK1k7JTf2EVvoirrjSzBaMhAnhi7//to8zvujDtgDZzy6aby75bAaDetlYPBq2brWehtrf9yDDG9WAMYJqp//scje/WmhbRR6eSdim1HaUcWk5+4ZPt8sQJcy8iWxQ4jtgjqTvMOe5v8ZPkxJNBine/ZKoJsv7FzKem00xEH7opzktaGukyEqH0VwOwKhmBiqsX2yN quentin@dufour.io"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBu+KUebaWwlugMC5fGbNhHc6IaQDAC6+1vMc4Ww7nVU1rs2nwI7L5qcWxOwNdhFaorZQZy/fJuCWdFbF61RCKGayBWPLZHGPsfqDuggYNEi1Qil1kpeCECfDQNjyMTK058ZBBhOWNMHBjlLWXUlRJDkRBBECY0vo4jRv22SvSaPUCAnkdJ9rbAp/kqb497PTIb2r1l1/ew8YdhINAlpYQFQezZVfkZdTKxt22n0QCjhupqjfh3gfNnbBX0z/iO+RvAOWRIZsjPFLC+jXl+n7cnu2cq1nvST5eHiYfXXeIgIwmeENLKqp+2Twr7PIdv22PnJkh6iR5kx7eTRxkNZdN quentin@deuxfleurs.fr"
    ];
    adrien = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINvUFN6HmZS5oxxOtmF6ug393m5NYbSbDI4G8pX6H9GZ adrien@pratchett"
    ];
    maximilien = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHMMR6zNzz8NQU80wFquhUCeiXJuGphjP+zNouKbn228GyESu8sfNBwnuZq86vblR11Lz8l2rtCM73GfAKg29qmUWUHRKWvRIYWv2vaUJcCdy0bAxIzcvCvjZX0SpnIKxe9y3Rp0LGO5WLYfw0ZFaavwFZP0Z8w1Kj9/zBmL2X2avbhkaYHi/C1yXhbvESYQysmqLa48EX/TS616MBrgR9zbI9AoTQ9NOHnR14Tve/AP/khcZoBJdm4hTttMbNkEc0wonzdylTDew263SPRs/uoqnQIpUtErdPHqU10Yup8HjXjEyFJsSwcZcM5sZOw5JKckKJwmcd0yjO/x/4/Mk5 maximilien@icare"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGioTNbjGE3KblbqhnkEWUfGkYZ2p5UAVqPdQJaUBWoo maximilien@athena"
    ];
    trinity = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWGWTRoF5MjQ5bmFdQENQlNdoYtA7Wd61GM0TMHZDki"
    ];
    baptiste = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnGkJZZrHIUp9q0DXmVLLuhCIe7Vu1J3j6dJ1z1BglqX7yOLdFQ6LhHXx65aND/KCOM1815tJSnaAyKWEj9qJ31RVUoRl42yBn54DvQumamJUaXAHqJrXhjwxfUkF9B73ZSUzHGADlQnxcBkmrjC5FkrpC/s4xr0o7/GIBkBdtZhX9YpxBfpH6wEcCruTOlm92E3HvvjpBb/wHsoxL1f2czvWe69021gqWEYRFjqtBwP36NYZnGOJZ0RrlP3wUrGCSHxOKW+2Su+tM6g07KPJn5l1wNJiOcyBQ0/Sv7ptCJ9+rTQNeVBMoXshaucYP/bKJbqH7dONrYDgz59C4+Kax"
    ];
    aeddis = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILoFf9fMYwLOpmiXKgn4Rs99YCj94SU1V0gwGXR5N4Md"
    ];
    boris = [
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPts/36UvMCFcx3anSMV8bQKGel4c4wCsdhDGWHzZHgg07DxMt+Wk9uv0hWkqLojkUbCl/bI5siftiEv6En0mHw="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJaD6flgTLkKimMB1qukiLKLVqsN+gizgajETjTwbscXEP2Fajmqy+90v1eXTDcGivmTyi8wOqkJ0s4D7dWP7Ck="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEIZKA/SIicXq7HPFJfumrMc1iARqA1TQWWuWLrguOlKgFPBVym/IVjtYGAQ/Xtv4wU9Ak0s+t9UKpQ/K38kVe0="
    ];
    vincent = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEve02acr522psrPxeElkwIPw2pc6QWtsUVZoaigqwZZ"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL/h+rxR2o+vN0hUWQPdpO7YY9aaKxO3ZRnUh9QiKBE7"
    ];
    armael = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJOoPghSM72AVp1zATgQzeLkuoGuP9uUTTAtwliyWoix"
    ];
    marion = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKzOhSTEOudBWCHi5wHc6MP0xjJJhuIDZEcx+hP6kz9N"
    ];
    darkgallium = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJX0A2P59or83EKhh32o8XumGz0ToTEsoq89hMbMtr7h"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB540H9kn+Ocs4Wjc1Y3f3OkHFYEqc5IM/FiCyoVVoh3"
    ];
    kokakiwi = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPTsEgcOtb2bij+Ih8eg8ZqO7d3IMiWykv6deMzlSSS kokakiwi@kira"
    ];
  };

  # For Garage external communication
  networking.firewall.allowedTCPPorts = [ 3901 ];

  # All prod nodes were deployed on the same version.
  # This could be put in individual node .nix files if we deploy
  # newer nodes on a different system version, OR we can bump this
  # regularly cluster-wide
  system.stateVersion = "21.05";
}
