# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "df-ykl";
  deuxfleurs.staticIPv4.address = "192.168.5.117";
  deuxfleurs.staticIPv6.address = "2a02:a03f:6510:5102:6e4b:90ff:fe3b:e86c";
  deuxfleurs.isRaftServer = true;

  fileSystems."/mnt" = {
    device = "/dev/disk/by-uuid/f7aa396f-23d0-44d3-89cf-3cb00bbb6c3b";
    fsType = "xfs";
    options = [ "nofail" ];
  };
}
