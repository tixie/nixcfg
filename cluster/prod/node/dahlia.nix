# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "dahlia";
  deuxfleurs.staticIPv4.address = "192.168.1.11";
  deuxfleurs.staticIPv6.address = "2a01:e0a:28f:5e60::11";
}
