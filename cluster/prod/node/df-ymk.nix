# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "df-ymk";
  deuxfleurs.staticIPv4.address = "192.168.5.116";
  deuxfleurs.staticIPv6.address = "2a02:a03f:6510:5102:6e4b:90ff:fe3b:e939";

  fileSystems."/mnt" = {
    device = "/dev/disk/by-uuid/51d95b17-0e06-4a73-9e4e-ae5363cc4015";
    fsType = "xfs";
    options = [ "nofail" ];
  };
}
