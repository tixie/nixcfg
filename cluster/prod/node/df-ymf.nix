# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "df-ymf";
  deuxfleurs.staticIPv4.address = "192.168.5.134";
  deuxfleurs.staticIPv6.address = "2a02:a03f:6510:5102:6e4b:90ff:fe3a:6174";

  fileSystems."/mnt" = {
    device = "/dev/disk/by-uuid/fec20a7e-5019-4747-8f73-77f3f196c122";
    fsType = "xfs";
    options = [ "nofail" ];
  };
}
