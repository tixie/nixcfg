# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/nvme0n1"; # or "nodev" for efi only

  deuxfleurs.hostName = "doradille";
  deuxfleurs.staticIPv4.address = "192.168.1.13";
  deuxfleurs.staticIPv6.address = "2a01:e0a:28f:5e60::13";
}
