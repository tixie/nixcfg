# Deuxfleurs on NixOS!

This repository contains code to run Deuxfleurs' infrastructure on NixOS.

## Our abstraction stack

We try to build a generic abstraction stack between our different resources (CPU, RAM, disk, etc.) and our services (Chat, Storage, etc.), we develop our own tools when needed.

Our first abstraction level is the NixOS level, which installs a bunch of standard components:

  * **Wireguard:** provides encrypted communication between remote nodes
  * **Nomad:** schedule containers and handle their lifecycle
  * **Consul:** distributed key value store + lock + service discovery
  * **Docker:** package, distribute and isolate applications
  
Then, inside our Nomad+Consul orchestrator, we deploy a number of base services:

  * Data management
    * **[Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage/):** S3-compatible lightweight object store for self-hosted geo-distributed deployments
    * **Stolon + PostgreSQL:** distributed relational database
  * Network Control Plane
    * **[DiploNAT](https://git.deuxfleurs.fr/Deuxfleurs/diplonat):** - network automation (firewalling, upnp igd)
    * **[D53](https://git.deuxfleurs.fr/lx/d53)** - update DNS entries (A and AAAA) dynamically based on Nomad service scheduling and local node info
    * **[Tricot](https://git.deuxfleurs.fr/Deuxfleurs/tricot)** - a dynamic reverse proxy for nomad+consul inspired by traefik
    * **[wgautomesh](https://git.deuxfleurs.fr/Deuxfleurs/wgautomesh)** - a dynamic wireguard mesh configurator
  * User Management
    * **[Bottin](https://git.deuxfleurs.fr/Deuxfleurs/bottin):** authentication and authorization (LDAP protocol, consul backend)
    * **[Guichet](https://git.deuxfleurs.fr/Deuxfleurs/guichet):** a dashboard for our users and administrators7
  * Observability
    * **Prometheus + Grafana:** monitoring

Some services we provide based on this abstraction:

  * **Websites:** Garage (static) + fediverse blog (Plume)
  * **Chat:** Synapse + Element Web (Matrix protocol)
  * **Email:** Postfix SMTP + Dovecot IMAP + opendkim DKIM + Sogo webmail  | Alps webmail (experimental)
    - **[Aerogramme](https://git.deuxfleurs.fr/Deuxfleurs/aerogramme/):** an encrypted IMAP server
  * **Visioconference:** Jitsi
  * **Collaboration:** CryptPad

As a generic abstraction is provided, deploying new services should be easy.

## How to use this?

See the following documentation topics:

- [Quick start and onboarding for new administrators](doc/onboarding.md)
- [How to add new nodes to a cluster (rapid overview)](doc/adding-nodes.md)
- [Architecture of this repo, how the scripts work](doc/architecture.md)
- [List of TCP and UDP ports used by services](doc/ports)
- [Why not Ansible?](doc/why-not-ansible.md)

## Got personal services in addition to Deuxfleurs at home?

Go check [`cluster/prod/register_external_services.sh`](./cluster/prod/register_external_services.sh). In bash, we register a redirect from Tricot to your own services or your personal reverse proxy.