{
  pkgs ? import <nixpkgs> {}
}:
pkgs.buildGoModule rec {
  pname = "nomad-driver-nix2";
  version = "0.1.0";

  src = builtins.fetchGit {
    url = "https://git.deuxfleurs.fr/Deuxfleurs/nomad-driver-nix2";
    rev = "98871a2d786f2627f1699d0ae49e7d6d3792f469";
  };

  vendorHash = "sha256-EQdTxVOBRYQjg7kAB+pFJYcNwk0zlsjjJxM+EO/cK84=";

  postInstall = ''
    mv $out/bin/nomad-driver-nix2 $out/bin/nix2
  '';

  meta = with pkgs.lib; {
    description = "Nomad driver to run Nix jobs";
    homepage = "https://git.deuxfleurs.fr/Deuxfleurs/nomad-driver-nix2";
    license = licenses.mpl20;
    platforms = platforms.linux;
  };
}
