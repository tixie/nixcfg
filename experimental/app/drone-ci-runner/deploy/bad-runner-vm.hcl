job "drone-runner" {
  datacenters = ["neptune"]
  type = "system"

  group "runner-vm" {
    network {
      port "ssh" {
        static = 22544
      }
    }

    task "drone-runner-vm" {
      driver = "qemu"

      config {
        image_path = "local/drone-runner.qcow2"
        accelerator = "kvm"
        args = [
          "-drive", "index=1,file=fat:rw:/var/lib/nomad/alloc/${NOMAD_ALLOC_ID}/${NOMAD_TASK_NAME}/secrets,format=raw,media=disk",
          "-device", "e1000,netdev=user.0",
          "-netdev", "user,id=user.0,hostfwd=tcp::${NOMAD_PORT_ssh}-:22",
          "-smp", "2",
        ]
        port_map {
          ssh = 22
        }
      }

      artifact {
        source = "https://alex.web.deuxfleurs.fr/drone-runner.qcow2.zst"
        destination = "local/drone-runner.qcow2"
        mode = "file"
      }

      template {
        data = <<EOH
DRONE_RPC_SECRET={{ key "secrets/drone-ci/rpc_secret" | trimSpace }}
DRONE_RUNNER_NAME={{ env "attr.unique.hostname" }}
EOH
        destination = "secrets/secret_env"
      }

      resources {
        memory = 2000
      }
    }
  }
}
