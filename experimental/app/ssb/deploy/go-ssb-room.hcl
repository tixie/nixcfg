job "ssb" {
  datacenters = ["neptune"]
  type = "service"

  group "go-ssb-room" {
    count = 1

    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "caribou"
    }

    network {
      port "web_port" { to = 3888 }
      port "ssb_port" { to = 8008 }
    }

    task "go-ssb-room" {
      driver = "docker"
      config {
        image = "lxpz/amd64_go_ssb_room:1"
        readonly_rootfs = true
        ports = [ "web_port", "ssb_port" ]
        network_mode = "host"
        command = "/app/cmd/server/server"
        args = [ 
          "-https-domain=ssb.staging.deuxfleurs.org",
          "-repo=/repo",
          "-aliases-as-subdomains=false",
          "-lishttp=:3888",
        ]
        volumes = [
          "/mnt/ssd/go-ssb-room:/repo"
        ]
      }

      resources {
        memory = 200
      }

      service {
        name = "go-ssb-room-http"
        tags = [
          "tricot ssb.staging.deuxfleurs.org",
        ]
        port = "web_port"
        address_mode = "driver"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "go-ssb-room-ssb"
        tags = [
          "(diplonat (port 8008))",
        ]
        port = "ssb_port"
        address_mode = "driver"
        check {
          type = "tcp"
          port = "ssb_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

