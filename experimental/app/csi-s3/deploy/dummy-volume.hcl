id           = "dummy-volume"
name         = "dummy-volume"
type         = "csi"
plugin_id    = "csi-s3"

capability {
  access_mode     = "single-node-writer"
  attachment_mode = "file-system"
}

secrets {
  accessKeyId = "GKfd94f06139bb73de5642baf5"
  secretAccessKey = "a4fa6c956d847b145a823c4615e4655126c67babf3cce2337b4d73cd381d7f06"
  endpoint = "https://garage-staging.home.adnab.me"
  region = "garage-staging"
}

parameters {
  mounter = "rclone"
}
