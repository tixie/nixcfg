job "plugin-csi-s3-nodes" {
  datacenters = ["neptune", "pluton"]

  # you can run node plugins as service jobs as well, but this ensures
  # that all nodes in the DC have a copy.
  type = "system"

  group "nodes" {
    task "plugin" {
      driver = "docker"

      config {
        image = "ctrox/csi-s3:v1.2.0-rc.1"

        args = [
          "--endpoint=unix://csi/csi.sock",
          "--nodeid=${node.unique.id}",
          "--logtostderr",
          "--v=5",
        ]

        # node plugins must run as privileged jobs because they
        # mount disks to the host
        privileged = true
      }

      csi_plugin {
        id        = "csi-s3"
        type      = "node"
        mount_dir = "/csi"
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}
